{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Interactive plots (II)\n",
    "\n",
    "Note for March 2021 course: This notebook requires JupyterLab 3.\n",
    "\n",
    "... continues [plotting_1.ipynb](plotting_1.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"toc\">*Table of contents*</a>\n",
    "\n",
    "- [Widgets](#Widgets)\n",
    "  - [Sliders](#Sliders)\n",
    "  - [Radio buttons](#Radio-buttons)\n",
    "  - [Dropdown menu](#Dropdown-menu)\n",
    "  - [Buttons](#Buttons)\n",
    "  - [Combining widgets](#Combining-widgets)\n",
    "  - [Debugging a callback function: the Output widget](#Debugging-a-callback-function:-the-Output-widget)\n",
    "  - [The callback function's argument](#The-callback-function's-argument)\n",
    "    - [Sharing a callback function](#Sharing-a-callback-function)\n",
    "- [A shortcut to widgets: ipywidgets.interact()](#A-shortcut-to-widgets:-ipywidgets.interact())\n",
    "- [Styling widgets ...](#Styling-widgets-...)\n",
    "- [Exercise](#excs1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Widgets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You see that we already can update the Bokeh figure \"in-place\", i.e. without re-drawing it fully, by working with its data source and the `push_notebook()` function. Now we want to learn how to use widgets to build small graphical user interfaces (GUIs) that trigger the updates."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notebook widgets are provided by the `ipywidgets`package:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import ipywidgets as widgets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can find a full list of the available widgets at [&rarr; ipywidgets.readthedocs.io](https://ipywidgets.readthedocs.io/en/latest/examples/Widget%20List.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a simple textbox:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "text_output = widgets.Text(description='Output:')\n",
    "text_output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I'll create another one to show you how to link widgets to specific actions, so-called *callback functions*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "text_input = widgets.Text(description=\"Input:\")\n",
    "\n",
    "\n",
    "def callback_function(change):\n",
    "    text_output.value = text_input.value\n",
    "\n",
    "\n",
    "text_input.observe(callback_function, 'value')\n",
    "text_input"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Type into the *Input* text field and observe the *Output* text field."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is how this works:\n",
    "\n",
    "By calling the `observe()` method of the `text_input` widget with the arguments `callback_function` and `'value'`, we tell the widget to look out for changes of its `value` attribute and to call the `callback_function()` if any change occurs. All the `callback_function()` does is to exchange the `value` of `text_output` widget with the `value` of the `text_input` widget. We can see this change live as we type thanks to the JavaScript machinery that powers Jupyter. Note that the `observe()` method can be configured to look for other changes too, not just the widget's value, see its documentation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the next part, to better follow what is happening, right-click the `text_output` text field and choose *Create New View for Output* (JupyterLab only)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are some more widgets that you can use to build GUIs:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sliders"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int_slider = widgets.IntSlider(min=0, max=20, step=2)\n",
    "\n",
    "\n",
    "def update_from_int_slider(change):\n",
    "    text_output.value = str(int_slider.value)\n",
    "\n",
    "\n",
    "int_slider.observe(update_from_int_slider, 'value')\n",
    "int_slider"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "float_slider = widgets.FloatSlider(min=0, max=10, step=0.1)\n",
    "\n",
    "\n",
    "def update_from_float_slider(change):\n",
    "    text_output.value = str(float_slider.value)\n",
    "\n",
    "\n",
    "float_slider.observe(update_from_float_slider, 'value')\n",
    "float_slider"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "float_range_slider = widgets.FloatRangeSlider(min=0, max=10, step=0.1)\n",
    "\n",
    "\n",
    "def update_from_float_range_slider(change):\n",
    "    text_output.value = str(float_range_slider.value)\n",
    "\n",
    "\n",
    "float_range_slider.observe(update_from_float_range_slider, 'value')\n",
    "float_range_slider"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Radio buttons"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "radio_input = widgets.RadioButtons(options=['foo', 'bar', 'baz'])\n",
    "\n",
    "\n",
    "def update_from_radio_buttons(change):\n",
    "    text_output.value = radio_input.value\n",
    "\n",
    "\n",
    "radio_input.observe(update_from_radio_buttons, 'value')\n",
    "radio_input"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dropdown menu"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dropdown_input = widgets.Dropdown(options=['foo', 'bar', 'baz'])\n",
    "\n",
    "\n",
    "def update_from_dropdown(change):\n",
    "    text_output.value = dropdown_input.value\n",
    "\n",
    "\n",
    "dropdown_input.observe(update_from_dropdown, 'value')\n",
    "dropdown_input"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Buttons"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "button = widgets.Button(description=\"Explode >:-]\")\n",
    "\n",
    "\n",
    "def update_from_button(caller):\n",
    "    text_output.value = \"Boom!\"\n",
    "\n",
    "\n",
    "# for buttons, you register a callback function\n",
    "# with the 'on_click' method\n",
    "button.on_click(update_from_button)\n",
    "button"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "toggle_button = widgets.ToggleButton(description=\"switch\")\n",
    "\n",
    "\n",
    "def update_from_toggle_button(change):\n",
    "    text_output.value = str(toggle_button.value)\n",
    "\n",
    "\n",
    "toggle_button.observe(update_from_toggle_button, 'value')\n",
    "toggle_button"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Combining widgets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can put widgets into \"boxes\" to arrange and display them together (similar to Bokeh layouts):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "widget_layout = widgets.VBox([\n",
    "    widgets.HBox([text_output]),\n",
    "    widgets.HBox([float_slider, button, toggle_button])\n",
    "])\n",
    "\n",
    "widget_layout"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Debugging a callback function: the Output widget"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Working with widgets has one very nasty property: Errors pass silently.\n",
    "\n",
    "Consider this example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "button_fails = widgets.Button(description=\"Display value\")\n",
    "\n",
    "\n",
    "def failing_callback_function(caller):\n",
    "    a = [1, 2, 3]\n",
    "    text_output.value = a[3]  # this will raise an IndexError\n",
    "\n",
    "\n",
    "button_fails.on_click(failing_callback_function)\n",
    "\n",
    "widgets.HBox([text_output, button_fails])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Clicking the *Display value* button seems to do nothing. This is because the lookup of the item with index 3 fails with an `IndexError` (recall that in Python the index starts at 0).\n",
    "Output from callback functions is not directed to the notebook, so we never get to see the error message. If we want the output, we have to define a target for it explicitly. We can do this with the [&rarr; Output widget](https://ipywidgets.readthedocs.io/en/latest/examples/Output%20Widget.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output = widgets.Output()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the `capture()` method of the `output` widget as a decorator for our callback function to redirect the callback function's output. Execute the next cell and try clicking the button:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "button_fails = widgets.Button(description=\"Display value\")\n",
    "\n",
    "\n",
    "@output.capture()\n",
    "def failing_callback_function(caller):\n",
    "    a = [1, 2, 3]\n",
    "    text_output.value = a[3]  # this will raise a IndexError\n",
    "\n",
    "\n",
    "button_fails.on_click(failing_callback_function)\n",
    "\n",
    "widgets.VBox([\n",
    "    widgets.HBox([text_output, button_fails]),\n",
    "    widgets.HBox([output])  # you will only see this\n",
    "])                          # once we redirected some output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we go, now you can see the error message."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `output` widget can be used in several other ways:\n",
    "\n",
    "We can use it as a context manager ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with output:\n",
    "    print('foo')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... directly append to it ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import sleep\n",
    "\n",
    "# something like this may be nice to have some feedback\n",
    "# on an expensive calculation:\n",
    "output.append_stdout('\\nthis may take a while ')\n",
    "\n",
    "for _ in range(10):\n",
    "    output.append_stdout('.')\n",
    "    sleep(0.5)  # some heavy calculation going on here ...\n",
    "\n",
    "output.append_stdout(' done!')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output.append_stderr('oops!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and clear it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output.clear_output()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The callback function's argument\n",
    "\n",
    "You may have recoginzed that in the examples above, the callback function takes an argument, which I called `change` or `caller`. This argument was never used so far. What is up with that?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameter `change` is described in the docstring of the `observe()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(text_output.observe)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameter `caller` is described in the docstring of the `on_click()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(button.on_click)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The help for `on_click` is fairly clear:\n",
    "\n",
    "> *The callback will be called with one argument, the clicked button widget instance.*\n",
    "\n",
    "The argument is the widget which called the function, so I called it `caller` (if that makes any sense 🙂)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The help for `observe` is somewhat abstract, so lets have a closer look what is inside the `change` parameter using the output widget once more (execute and type into the text field):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pprint import pprint  # pretty printing from the standard libary\n",
    "\n",
    "output_change_parameter = widgets.Output()\n",
    "\n",
    "# clear output after each function call: clear_output=True\n",
    "@output_change_parameter.capture(clear_output=True)\n",
    "def print_change_parameter(change):\n",
    "    pprint(change)\n",
    "\n",
    "\n",
    "text_input.observe(print_change_parameter, 'value')\n",
    "\n",
    "widgets.VBox([text_input, output_change_parameter])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This should make the parameter `change` clear. It is a dictionary with the keys:\n",
    "\n",
    "- `name`: the name of the widget's attribute that was modified\n",
    "- `type`: the type of the modification (here, a simple *change*)\n",
    "- `old`, `new`: the new and old value of the modified attribute\n",
    "- `owner`: the widget that called the callback function (the owner of the attribute that was changed)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sharing a callback function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we see that the separate callback functions I defined above are needlessly redundant. Let's do this again, this time using the parameter of the callback function to avoid this redundancy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define the widgets\n",
    "\n",
    "# input\n",
    "text_input = widgets.Text(description=\"Input:\")\n",
    "float_slider = widgets.FloatSlider(min=0, max=10, step=0.1)\n",
    "radio_input = widgets.RadioButtons(options=['foo', 'bar', 'baz'])\n",
    "button = widgets.Button(description=\"Explode >:-]\")\n",
    "\n",
    "# output\n",
    "text_output = widgets.Text(description='Output:')\n",
    "\n",
    "\n",
    "# define the callback function\n",
    "def flexible_callback_function(change_or_caller):\n",
    "\n",
    "    if change_or_caller == button:\n",
    "        text_output.value = 'Boom!'\n",
    "    else:\n",
    "        text_output.value = str(change_or_caller['new'])\n",
    "\n",
    "\n",
    "# link the widgets to the callback function\n",
    "widget_list = [text_input, float_slider, radio_input]\n",
    "\n",
    "for w in widget_list:\n",
    "    w.observe(flexible_callback_function, 'value')\n",
    "\n",
    "button.on_click(flexible_callback_function)\n",
    "\n",
    "# display the widgets\n",
    "widgets.VBox([\n",
    "    widgets.HBox([text_output]),\n",
    "    widgets.HBox([button] + widget_list)\n",
    "])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A shortcut to widgets: ipywidgets.interact()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Defining widgets and callback functions can sometimes be abbreviated by using the [&rarr; `interact()` function](https://ipywidgets.readthedocs.io/en/latest/examples/Using%20Interact.html):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ipywidgets import interact"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`interact()` is helpful especially if you want to play with a simple function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's say we want to display the result of a binary operation of two numbers. Doing this as introduced above would require four widgets&mdash;two to choose the numbers, one to choose the operation, one to display the result&mdash;and a callback function.\n",
    "With `interact()`, we only need to define the function that we want to play with, the required widgets are generate automatically. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def combine_a_b(a, b, operation):\n",
    "\n",
    "    if operation == 'add':\n",
    "        result_str = f\"{a} + {b} = {a + b}\"\n",
    "    elif operation == 'subtract':\n",
    "        result_str = f\"{a} - {b} = {a - b}\"\n",
    "    elif operation == 'multiply':\n",
    "        result_str = f\"{a} * {b} = {a * b}\"\n",
    "    elif operation == 'divide':\n",
    "        result = \"NaN\" if b == 0 else a / b\n",
    "        result_str = f\"{a} / {b} = {result}\"\n",
    "    else:\n",
    "        result_str = \"operation unknown\"\n",
    "\n",
    "    print(result_str)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "combine_a_b(4, 2, 'add')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "combine_a_b(43, 0, 'divide')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calling `interact()` and passing the arguments of `combine_a_b()` as keyword arguments with specific values (see below) yields the desired GUI:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "interact(combine_a_b,\n",
    "         a=(0, 10, 0.1),\n",
    "         b=(0, 10, 1),\n",
    "         operation=['add', 'subtract', 'multiply', 'divide', 'modulus']);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we could have set `text_output.value = result_str` in `combine_a_b()` to get a nicer output, but `interact()` takes care of catching and displaying the output, so we don't even need to define or work with an output widget."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The \"magic\" of `interact()` is that it automatically translates the keyword arguments (`a=(0, 10, 0.1)`, `b=(0, 10, 1)` and `operation=['add', 'subtract', 'multiply', 'divide', 'modulus']`) into sensible widgets. Here is another example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "interact(combine_a_b,\n",
    "         a=True,\n",
    "         b=False,\n",
    "         operation='add');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This saves us a lot of typing. However, if we wish, we can still pass widgets to state specifically what we want:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "interact(combine_a_b,\n",
    "         a=(0, 10, 0.1),\n",
    "         b=(0, 10, 1),\n",
    "         operation=widgets.RadioButtons(options=['add', 'subtract', 'multiply', 'divide']));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Styling widgets ...\n",
    "\n",
    "... is a topic that I want to exclude here. You can find more about how to make your widgets nice and shiny [&rarr; here](https://ipywidgets.readthedocs.io/en/latest/examples/Widget%20Styling.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"excs1\"></a>\n",
    "## Exercise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a simple Bokeh figure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bokeh.plotting import figure, ColumnDataSource\n",
    "\n",
    "bokeh_figure = figure(height=400,\n",
    "                      title='f(x) = ...',\n",
    "                      x_axis_label='x',\n",
    "                      y_axis_label='y',\n",
    "                      y_range=(-10, 10),)\n",
    "\n",
    "bokeh_figure.title.text_font_size = '14pt'\n",
    "data_source = ColumnDataSource(dict(x=[], y=[]))\n",
    "bokeh_figure.line(x='x', y='y', source=data_source);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use widgets to interact with it as shown here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Video\n",
    "Video(\"figures/widget-exercise.mp4\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&rarr; next notebook: plotting_3.ipynb](plotting_3.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
