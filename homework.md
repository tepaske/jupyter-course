# Homework: Build a Reproducible Project

## Homework: Build a Reproducible Project (1/2)

On <https://jupyter-cloud.gwdg.de> ...

- create a new project (see [environments.ipynb → Project creation workflow](environments.ipynb#Project-creation-workflow)) project includes its own environment, requirements.txt, and README.md
  - `pip install` at least Numpy and Matplotlib as dependencies
- use git version control to track changes of your project
  - `git init` right after you created your project
  - exclude your environment from the git repo (see [version-control.ipynb → Ignoring files](version-control.ipynb#Ignoring-files))
- create a notebook, make a plot, and use Markdown to add a description
  - (**optional**) load data and/or perform data transformation/evaluation
- (**optional**) add more content to your repo, e.g. a Python module with your
  own function that you import in the notebook

---

## Homework: Build a Reproducible Project (2/2)

- make commits after logical units of work (e.g. after you finish the notebook,
  add content to the README.md, add a new function to the module, etc.)
- create a remote repository and push your project (<https://gitlab.gwdg.de>)
- clone your project in another folder on <https://jupyter-cloud.gwdg.de>,
  install it, and verify that it runs as intended (**optionally** do this on
  your local computer)
- add me as a member of your gitlab project (*Project information* → *Members*
  → *Invite members*) so I can have a look and try to run it.

  ---
