{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "import numpy as np\n",
    "from holoviews import opts"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hv.extension(\"bokeh\")\n",
    "\n",
    "opts.defaults(\n",
    "    opts.Slope(color=\"red\"),\n",
    "    opts.VLine(color=\"red\"),\n",
    "    opts.HLine(color=\"red\"),\n",
    "    opts.Scatter(color=\"green\"),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# HoloViews Basics\n",
    "\n",
    "In this notebook we are looking at the core concepts of how the package \"HoloViews\" works.\n",
    "\n",
    "HoloViews is a high level plotting and data manipulation library that offers a fast workflow and easy to achieve interactivity.\n",
    "\n",
    "If the following introduction gets you interested, you can learn more at https://holoviews.org."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## HoloViews Concept"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Represent data as a plot by default.*\n",
    "\n",
    "How?\n",
    "\n",
    "- Define element type of data (required)\n",
    "- Providing metadata to refine visual representation (optional)\n",
    "- Set options to fine-tune appearance (optional)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Elements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Elements* define the \"type\" of your data.\n",
    "\n",
    "A few examples:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**`hv.Curve()`**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "time = np.linspace(0, 2 * np.pi, 100)\n",
    "signal = np.sin(time)\n",
    "sine_curve = hv.Curve((time, signal))\n",
    "sine_curve"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**`hv.Scatter()`**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "weight = np.array([0.1, 1, 2.5, 2.9, 5.0, 6.0, 7.5, 10.0])\n",
    "deviation = np.array([-0.007, 0.007, 0.008, 0.010, 0.015, 0.028, 0.031, 0.043])\n",
    "scale_calibration = hv.Scatter((weight, deviation))\n",
    "scale_calibration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**`hv.Bars()`**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grade = [\"A\", \"B\", \"C\", \"D\"]\n",
    "count = [5, 12, 7, 2]\n",
    "exam_outcome = hv.Bars((grade, count))\n",
    "exam_outcome"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**`hv.Image()`**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y = np.arange(-2, 2, 0.1), np.arange(-2, 2, 0.1)\n",
    "X, Y = np.meshgrid(x * np.pi, y * np.pi)\n",
    "Z = np.cos(X) * np.sin(Y)\n",
    "wave = hv.Image((x, y, Z))\n",
    "\n",
    "wave"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note how you have to provide the data using tuples (`(x, y)`)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Composing elements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create a *layout* (subplots) by \"adding\" elements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cosine_curve = hv.Curve((time, np.cos(time)))\n",
    "sine_curve + cosine_curve"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create *overlays* by \"multiplying\" elements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sine_curve * cosine_curve"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can overaly several elements, not just two:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wave * hv.VLine(1.0) * hv.HLine(\n",
    "    0.5\n",
    ")  # VLine/HLine elements draw vertical/horizontal lines"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Overlays and layouts can be combined:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "slope, intercept = np.polyfit(weight, deviation, 1)\n",
    "residuals = deviation - np.polyval([slope, intercept], weight)\n",
    "\n",
    "scale_fit_layout = scale_calibration * hv.Slope(slope, intercept) + hv.Scatter(\n",
    "    (weight, residuals)\n",
    ")\n",
    "\n",
    "scale_fit_layout"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note how the `hv.Slope` element adapts automatically to the overlayed scatter plot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The arrangement of elements in a layout can be controlled by choosing a number of columns:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scale_fit_layout.cols(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 1\n",
    "\n",
    "Plot the following data \n",
    "1. next to each other\n",
    "2. on top of each other"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xa = np.linspace(-5, 5, 100)\n",
    "ya = 2 * xa + 5 + 5 * np.random.rand(xa.size)\n",
    "xb = np.linspace(-6, 4, 100)\n",
    "yb = 1.5 * xb + 7 + 5 * np.random.rand(xb.size)\n",
    "\n",
    "# your plot:\n",
    "# ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Metadata\n",
    "\n",
    "### Dimensions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may have noticed that all plots up to now are labelled merely with *x* and *y*.\n",
    "In particular the last plot (`scale_fit_layout`) is confusing without labels.\n",
    "Also: have you noticed the strange scaling on the plot of the residuals?\n",
    "It follows the y-scaling of the upper plot, but that does not make sense here ...\n",
    "\n",
    "Let's fix these issues!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What we want to do is to provide information about the *dimensions* of our data.\n",
    "\n",
    "The simplest way to achieve this is to provide labels for the dimensions like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scale_calibration = hv.Scatter((weight, deviation), \"weight\", \"deviation\")\n",
    "\n",
    "scale_calibration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can provide more information about the dimensions using the `hv.Dimension` object, e.g. we can provide units:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scale_calibration = hv.Scatter(\n",
    "    (weight, deviation),\n",
    "    kdims=hv.Dimension(\"weight\", unit=\"kg\"),\n",
    "    vdims=hv.Dimension(\"deviation\", unit=\"kg\"),\n",
    ")\n",
    "\n",
    "scale_calibration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or we can add longer descriptions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scale_calibration = hv.Scatter(\n",
    "    (weight, deviation),\n",
    "    kdims=hv.Dimension(\"weight\", label=\"reference weight\", unit=\"kg\"),\n",
    "    vdims=hv.Dimension(\"deviation\", label=\"deviation from reference weight\", unit=\"kg\"),\n",
    ")\n",
    "\n",
    "scale_calibration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Noticed the `kdims` and `vdims` keyword arguments? These are abbreviations for **key dimensions** and **value dimensions**. Key dimensions are used for *independent variables*, value dimensions for the *dependent variables*. So when you plot the dependent variable $y$ versus the independent variable $x$, your key dimension is $x$ and the value dimension is $y$.\n",
    "\n",
    "Another way to but it is that for each item of a key dimension, there is an associated item in the value dimension:\n",
    "\n",
    "x₁ → y₁  \n",
    "country → population  \n",
    "car model → horse powers  \n",
    "etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using `Dimension` the plot of the fit and calibration data makes much more sense now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scale_fit_layout = scale_calibration * hv.Slope(slope, intercept) + hv.Scatter(\n",
    "    (weight, residuals),\n",
    "    kdims=hv.Dimension(\"weight\", label=\"reference weight\", unit=\"kg\"),\n",
    "    vdims=hv.Dimension(\"residuals\", unit=\"kg\"),\n",
    ")\n",
    "\n",
    "scale_fit_layout.cols(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Did you note how the scaling on the residuals plot changed? The y-axes of the plots are now not linked anymore, because they represent different dimensions. By default, the same dimension will use the same scaling in a layout."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dimensions can also be changed after creating an element, using `redim()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sine_curve = sine_curve.redim(x=\"time\", y=\"signal\")\n",
    "sine_curve"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that redim returns a new object, so we have to overwrite the old object (or declare new one), if we want to keep the result of `redim()`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Labels and groups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can label an element, e.g. upon creation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cosine_curve = hv.Curve((time, np.cos(time)), \"time\", \"signal\", label=\"cosine\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or after creation (relabelling):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sine_curve = sine_curve.relabel(\"sine\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Labels appear as plot titles in subplot layouts:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cosine_curve + sine_curve"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And, conveniently, they turn into legends in overlays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cosine_curve * sine_curve"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also assign a group to an element. It appears together with the label (layout):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sine_curve.relabel(group=\"signal\") + cosine_curve.relabel(group=\"signal\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or as a plot title (overlay):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_overlay = cosine_curve.relabel(group=\"wave\") * sine_curve.relabel(group=\"wave\")\n",
    "\n",
    "signal_overlay"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`group` can also be passed as a keyword argument when creating an element."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Options"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Element-wise (local options)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the `.opts()` element method and `hv.opts` module to more finely control the appereance of an element, for example, the size (note that I imported the `hv.opts` module into the global namespace by doing `from holoviews import opts`, so I can write `opts` instead of `hv.opts`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sine_curve.opts(\n",
    "    opts.Curve(\n",
    "        color=\"orange\",\n",
    "        frame_width=600,\n",
    "        hover_color=\"red\",\n",
    "        line_width=5,\n",
    "        tools=[\"hover\"],\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use tab-completion to discover the available options (place mouse cursor after `opts.Curve(` and press `tab` on your keyboard)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Options are persistent, i.e. if I call my `sine_curve` element again, it will look like I styled it above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sine_curve"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can style elements in a layout/overlay refering to their type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wavy_layout = signal_overlay + wave\n",
    "\n",
    "wavy_layout.opts(opts.Curve(frame_width=400, line_width=4, line_dash=\"dashed\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to apply options to a specific element in a layout/overlay, you can refer to its group and label:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wavy_layout.opts(opts.Curve(\"Wave.Sine\", line_dash=\"solid\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that you have to put in group and label as `Group.Label`, mind the **title case**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also access an item of a layout directly to apply options by using dot notation. To find a particular sub-element, it is helpful to `print()` the layout, which will show its structure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(wavy_layout)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, the layout is composed of an overlay `.Wave.I` and an image `.Image.I` and the overlay is composed of two curves, `.Wave.Cosine` and `.Wave.Sine`. To access and style the cosine curve, we can do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wavy_layout.Wave.I.Wave.Cosine.opts(color=\"green\")\n",
    "wavy_layout"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Default (global options)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can set default options too, for example, make your plots always appear a little bigger than they do by default, or choose a different default color. Scroll up and check the second input cell. I defined some default colors there using `opts.default()`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try to give our scatter plots a different marker and make it larger:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opts.defaults(\n",
    "    opts.Scatter(marker=\"x\", size=16, color=\"purple\", frame_width=300, frame_height=300)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All already existing and new scatter plots are affected:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scale_calibration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hv.Scatter((np.arange(10), np.random.randn(10)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Plot the data from Exercise 1 on top of each other. This time, use a group, label (keyword arguments `group` and `label`) and color (`.opts()`) to tell the two datasets appart.\n",
    "2. Use `hv.Dimension` and `.redim()` to redefine the dimensions to *time* in units of seconds for the key dimension and count in the value dimension."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Output and Backend"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may have noticed that I called a function called `hv.extension()` in the second code cell.\n",
    "\n",
    "What does it do?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`hv.extension()` activates a plotting \"extension\" for HoloViews to display your data as plots. It allows you to use different plotting libraries which do the actual plotting. In this context, the plotting libraries are called \"backends\". They do the actual work to produce the plots, but you do not have to worry about this, you only use the \"frontent\" (holoviews) and tell it what you want. It will call the backend for you to create the plot. Frontent/backend is a common notion in software engineering to categorize the layers of a program.\n",
    "\n",
    "HoloViews can use Bokeh, Matplotlib, and Plotly as plotting backends. This is very handy, because Matplotlib is better at producing static plots for publications while Bokeh and Plotly work better with interactive environments such as Jupyter, since they use HTML and JavaScript to create interactive plots that allow panning, zooming, and so on. (Matplotlib can do this too, but not as good in my experience.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Change the call to `hv.extension()` to\n",
    "\n",
    "```python\n",
    "hv.extension(\"matplotlib\")\n",
    "```\n",
    "\n",
    "and re-run the notebook. What happens?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To save a plot, we can use the `hv.save()` function. The keyword argument `backend` controls which library we want to use to create the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hv.save(wavy_layout, filename=\"tmp/wavy_layout.html\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hv.save(wavy_layout, filename=\"tmp/wavy_layout.png\", backend=\"matplotlib\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Have a look at the output files! Do you notice some differences?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 3\n",
    "\n",
    "Save your last figure from Exercise 2 to an SVG file using Matplotlib as a backend. Again, is there a difference to the plot that you see in this notebook?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we know the basics of plotting data with HoloViews, although we only scratched the surface.\n",
    "\n",
    "Let's go on and learn more about how we can work with a bit more complex data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&rarr; next notebook: holoviews_nD-data.ipynb](holoviews_nD-data.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solutions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exersice 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 1) plots next to each other with `+`\n",
    "hv.Scatter((xa, ya)) + hv.Scatter((xb, yb))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 2) plots next to each other with `*`\n",
    "hv.Scatter((xa, ya)) * hv.Scatter((xb, yb))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 1\n",
    "plot_a = hv.Scatter((xa, ya), group=\"dataset\", label=\"A\")\n",
    "plot_b = hv.Scatter((xb, yb), group=\"dataset\", label=\"B\")\n",
    "\n",
    "plot_b.opts(color=\"green\")\n",
    "\n",
    "plot_a * plot_b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 2\n",
    "plot_a = plot_a.redim(x=hv.Dimension(\"time\", unit=\"s\"), y=\"count\")\n",
    "plot_b = plot_b.redim(x=hv.Dimension(\"time\", unit=\"s\"), y=\"count\")\n",
    "\n",
    "plot_a * plot_b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 2 without redundant dimension definition\n",
    "dims = dict(x=hv.Dimension(\"time\", unit=\"s\"), y=\"count\")\n",
    "plot_a = plot_a.redim(**dims)\n",
    "plot_b = plot_b.redim(**dims)\n",
    "\n",
    "plot_a * plot_b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hv.save(plot_a * plot_b, filename=\"tmp/hv_1_exercise_3.svg\", backend=\"matplotlib\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The style attributes such as color and marker shape are not used by Matplotlib. Unfortunately, some options have different names for the different backends and do not work interchangeably."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
