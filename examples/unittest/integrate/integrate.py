"""
integrate.py

A sample module to demonstrate unit testing.

This module includes a single function which performs numerical
integration of x-y data using the trapezoidal rule.
"""


def integrate_trapezoidal(x, y, display_result=False):
    """Perform numerical integration based on the trapezoidal rule.

    Parameters
    ----------
    x, y : list or array of float or int
        The discrete x and y data.
    display_results : bool (optional, defaults to False)
        If True, the function will display the result.

    Returns
    -------
    integral : float
        The numerical integral.
    """
    zipped_data = zip(x[:-1], y[:-1], x[1:], y[1:])
    integral = sum([(yi + yj)/2 * (xj - xi) for xi, yi, xj, yj in zipped_data])
    if display_result:
        print(f"The integral is {integral:.3e}.")
    return integral
