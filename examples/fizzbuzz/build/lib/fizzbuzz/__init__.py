"""
FizzBuzz

author: N. Luettschwager <nluetts@gwdg.de>
"""
import argparse


def fizzbuzz(n):
    for i in range(1, n+1):
        if (i % 3 == 0) and (i % 5 == 0):
            print('FizzBuzz!')
        elif i % 3 == 0:
            print('Fizz')
        elif i % 5 == 0:
            print('Buzz')
        else:
            print(i)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("N", type=int, help="Run FizzBuzz for numbers 1 to N.")
    return parser.parse_args()

def main():
    args = parse_args()
    print("FizzBuzz for the numbers 1 ... 100")
    fizzbuzz(args.N)


if __name__ == '__main__':
    main()