"""
Helper functions for curve_fitting.ipynb
"""

import numpy as np

from bokeh.plotting import figure, ColumnDataSource

from scipy.stats import norm


DEFAULT_FIGURE_OPTIONS = {
    "title.text_font_size": "14pt",
    "xaxis.axis_label_text_font_size": "12pt",
    "yaxis.axis_label_text_font_size": "12pt",
    "xaxis.major_label_text_font_size": "12pt",
    "yaxis.major_label_text_font_size": "12pt",
    "legend.label_text_font_size": "10pt",
}

DEFAULT_FIGURE_KWARGS = {
    'height': 400,
    'width': 800,
}

DEFAULT_PLOT_KWARGS = {
    "square": {"color": "blue"},
    "line": {"line_width": 2, "color": "blue"},
    "multi_line": {"line_width": 1, "color": "blue"}
}


####################
# helper functions #
####################

def _get_nested_attr(obj, path):
    for a in path.split('.'):
        attr = getattr(obj, a, None)
        if attr is None:
            return None
        obj = attr
    return attr


def _set_nested_attr(obj, path, value):
    if not _get_nested_attr(obj, path):
        return
    parent_path, attr = path.rsplit('.', 1)
    parent = _get_nested_attr(obj, parent_path)
    setattr(parent, attr, value)


######################
# plotting functions #
######################

def _format_figure(fig):
    for property_name, value in DEFAULT_FIGURE_OPTIONS.items():
        _set_nested_attr(fig, property_name, value)


def set_default_figure_option(options):
    global DEFAULT_FIGURE_OPTIONS
    DEFAULT_FIGURE_OPTIONS.update(options)


def get_data_source(fig, name=None):
    if name is None:
        return fig.renderers[0].data_source
    for renderer in fig.renderers:
        if renderer.name == name:
            return renderer.data_source
    return None


def plot(x, y, plot_method="line", fig=None, data_source=None,
         return_data_source=False, reformat=True, **kwargs):
    """Generate Bokeh figure and plot data in one step.

    Combines Bokeh figure creation and plotting of a dataset
    into a single function call.
    Note: If no data source is provided, it will be generated
    from x and y.

    Parameters
    ----------
    x, y : array or str
        Data to plot or labels of data to plot from a Bokeh
        ColumnDataSource (if provided through `data_source`
        keyword argument.)
    plot_method : str (optional, defaults to 'line')
        Plotting method available to a Bokeh figure, e.g. 'line',
        'circle', or 'square'.
    fig : Bokeh figure object (optional)
        If provided, plot is added to this figure, otherwise
        a new figure is created.
    data_source : Bokeh ColumnDataSource object (optional)
        If provided, x and y have to be labels denoting
        which data in ColumnDataSource object to plot.
    return_data_source : bool (optional, defaults to False)
        If true, a ColumnDataSource object will be returned
        together with a Bokeh figure instance.
    reformat : bool (optional, defaults to True)
        If true, figure will be formated according to module
        default formatting (DEFAULT_FIGURE_OPTIONS)
    **kwargs : further keyword arguments
        Furhter keyword arguments are passed to the Bokeh
        figure's plotting method. Keyword arguments starting
        with 'figure_' are passed to Bokeh's figure function:
        >>> fig = figure(width=800, height=400)
        >>> fig.line(x, y, color="blue")
        is equivalent to:
        >>> plot(x, y, color="blue", figure_width=800, figure_height=400)

    Returns
    -------
    fig : Bokeh figure
    data_source : Bokeh ColumnDataSource (if `return_data_source=True`)
    """
    # parse keyword arguments
    plot_kwargs = DEFAULT_PLOT_KWARGS.get(plot_method, dict()).copy()
    plot_kwargs.update(
        {k: v for k, v in kwargs.items()
         if not k.startswith("figure_")})
    fig_kwargs = DEFAULT_FIGURE_KWARGS.copy()
    fig_kwargs.update(
        {k[7:]: v for k, v in kwargs.items()
         if k.startswith("figure_")})

    # prepare figure, if applicable
    if fig is None:
        fig = figure(**fig_kwargs)
    if reformat:
        _format_figure(fig)

    # prepare data source
    if data_source is None:
        data_source = ColumnDataSource(dict(x=x, y=y))
        x, y = 'x', 'y'

    # select and verify plot function
    plot_function = getattr(fig, plot_method, None)

    if plot_function is None:
        raise ValueError("Plot method not supported.")

    # prepare plot
    plot_function(x, y, source=data_source, **plot_kwargs)

    return (fig, data_source) if return_data_source else fig


def add_errorbar(x, y, fig=None, xerr=None, yerr=None,
                 return_data_source=False,
                 xerr_name=None, yerr_name=None, **kwargs):

    if xerr is not None:
        x_err_x, x_err_y = [], []
        for px, py, err in zip(x, y, xerr):
            x_err_x.append((px - err, px + err))
            x_err_y.append((py, py))
        plot_and_source = plot(
            x_err_x, x_err_y, fig=fig, plot_method="multi_line",
            return_data_source=return_data_source, name=xerr_name, **kwargs)

    if yerr is not None:
        y_err_x, y_err_y = [], []
        for px, py, err in zip(x, y, yerr):
            y_err_x.append((px, px))
            y_err_y.append((py - err, py + err))

        fig = plot_and_source if type(plot) != tuple else plot_and_source[0]

        plot_and_source = plot(
            y_err_x, y_err_y, plot_method="multi_line", fig=fig,
            return_data_source=return_data_source, name=yerr_name, **kwargs)

    return plot_and_source


############################
# data generator functions #
############################

def get_linear_sample_data():
    """Return linear x-y sample data.

    Sample dataset to demonstrate linear fitting.
    """
    np.random.seed(42)
    x = np.arange(10)
    y = 3 * x + 5 + np.random.randn(len(x))
    return x, y


def get_linear_sample_data_with_uncertainties():
    """Return linear x-y sample data with uncertainties.

    Sample dataset to demonstrate linear fitting.
    """
    x, y = get_linear_sample_data()
    np.random.seed(42)
    u_x = np.random.random(len(x))/10 + 0.2
    u_y = np.random.random(len(x))/2 + 2
    return x, y, u_x, u_y


def get_polynomial_sample_data():
    """Return 3rd order polynomial x-y sample data.

    Sample dataset to demonstrate polynomial fitting.
    """
    x = np.linspace(-5, 5, 200)
    y = np.polyval([0.2, 0.1, 1, 10], x)
    np.random.seed(42)
    noise = np.random.randn(len(y))
    return x, y + noise


# use the Gaussian propability density function from scipy.stats
# to simulate peaks
def _gauss_peak(grid, position, height, width):
    peak = norm.pdf(grid, loc=position, scale=width)
    return height * peak/peak.max()


def get_sample_spectrum():
    """
    Return simulated spectrum (x-y data).

    Sum of three Gaussian curves, sample dataset to demonstrate non-linear
    fitting.
    """

    grid = np.linspace(0, 50, 200)
    peak_parameters = [[10, 2.0, 1.5],  # position, height, width
                       [15, 3.0, 2.5],
                       [40, 0.5, 2.0]]

    spectrum = sum([_gauss_peak(grid, *p) for p in peak_parameters])
    np.random.seed(42)
    noise = np.random.randn(len(spectrum)) / 10
    return grid, spectrum + noise


def get_cmc_dataset():
    """Return x-y dataset from critical micelle concentration measurement.

    Data from Juan P. Marcolongo, Martín Mirenda,
    Thermodynamics of Sodium Dodecyl Sulfate (SDS) Micellization:
    An Undergraduate Laboratory Experiment
    Journal of Chemical Education 88 (2011), 629-633, DOI: 10.1021/ed900019u
    """

    concentration, conductivity = np.array([
        [0.27,  0.046],
        [2.06,  0.277],
        [3.20,  0.428],
        [4.76,  0.628],
        [6.18,  0.809],
        [11.03, 1.352],
        [14.51, 1.557],
        [18.40, 1.795],
        [21.89, 1.992],
        [24.91, 2.158],
    ]).T

    return concentration, conductivity
