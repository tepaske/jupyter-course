{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Working with tabular data (II)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... continues [tabular-data_1.ipynb](tabular-data_1.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"tocontents\">*Table of contents*</a>\n",
    "\n",
    "- [Selecting data](#Selecting-data)\n",
    "  - [Fancy indexing](#Fancy-indexing)\n",
    "    - [Combining filter conditions](#Combining-filter-conditions)\n",
    "    - [Exercise 1](#excs1)\n",
    "  - [Using the query method](#Using-the-query-method)\n",
    "    - [Exercise 2](#excs2)\n",
    "  - [Further reading](#Further-reading)\n",
    "- [Sorting dataframes](#Sorting-dataframes)\n",
    "- [Analyzing data](#Analyzing-data)\n",
    "  - [Statistics](#Statistics)\n",
    "  - [Grouping data](#Grouping-data)\n",
    "  - [Plotting](#Plotting)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from script import prepare_who_data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data = prepare_who_data.get_WHO_data()\n",
    "WHO_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Selecting data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fancy indexing\n",
    "\n",
    "We would like to investigate the data with respect to, for example, specific countries, regions, or time spans. To do this, we need to know how to select *subsets*. One way to do this is to use *fancy indexing*, much like in Numpy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fancy indexing means that we pass a series which holds boolean values that state whether a specific conditions is true or false. Watch how we select the subset which contains only data for Germany:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mask = WHO_data[\"Country\"] == \"Germany\"\n",
    "mask  # we say we use the boolean series as a \"mask\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(note that \"Germany\" is outside the range of displayed entries, thus all we see are `False` values)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data[mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In much the same way, we could ask for the data for a specific year ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data[WHO_data[\"Year\"] == 2010]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "or a specific region:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data[WHO_data[\"Region\"] == \"European Region\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tocontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Combining filter conditions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the `&` (*and*) and the `|` (*or*) operator to combine boolean series (masks):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# selecting France and Germany\n",
    "mask = (WHO_data[\"Country\"] == \"Germany\") | (WHO_data[\"Country\"] == \"France\")\n",
    "WHO_data[mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mind the parenthesis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"excs1\"></a>\n",
    "#### Exercise 1\n",
    "\n",
    "- Select all data where the life expectancy of both sexes is at least 80 years.\n",
    "- Select all data from the US (`\"United States of America\"`) newer than 2010."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tocontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using the query method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Except fancy indexing, we can also use the `.query()` method to select subsets of a dataframe. In my opinion, this can sometimes lead to more readable code.\n",
    "\n",
    "A query is provided as a string and uses its own mini-language. For example, selecting Germany with `.query()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"Country == 'Germany'\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that Pandas interprets `Country` as a column name but `'Germany'` (in single quotes) as a string."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also compare two columns with each other, e.g. to find the data were the gap between female and male life expectancy is not more than one year:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"(Female - Male) <= 1\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the column name contains a space, we have to use back ticks:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"`Both sexes` >= 80\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also use variables in queries by preceding their names with the `@` symbol:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "min_expenditure = 8000\n",
    "WHO_data.query(\"Expenditure >= @min_expenditure\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Combining queries works as you might expect:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"Expenditure >= 500 and Region == 'African Region'\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"excs2\"></a>\n",
    "#### Exercise 2\n",
    "\n",
    "- use `.query()` to select all data where the expenditure was at least 500 US\\$ in the \"African Region\" and the \"Eastern Mediterranean Region\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tocontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Further reading\n",
    "\n",
    "Look at the [&rarr; Pandas indexing and selecting user guide](https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html) for more information. Especially the [&rarr; `.isin()` method](https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#indexing-with-isin) is very handy to avoid chaining a lot of *or* conditions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sorting dataframes\n",
    "\n",
    "You can sort a dataframe by a specific column/specific columns with the `.sort_values()` method.\n",
    "\n",
    "Who had the longest life expectancy in 2010?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"Year == 2010\").sort_values(\"Both sexes\", ascending=False).head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or the lowest expenditure in the European Region?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"Region == 'European Region' and Year == 2010\").sort_values(\"Expenditure\").head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tocontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analyzing data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With querying the dataframe alone, we can already answer quite some interesting questions.\n",
    "If we want to be more quantitative, however, we have to calculate statistics.\n",
    "Let's see what Pandas has in store for us."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Statistics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pandas has methods for all common statistics, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.mean(numeric_only=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.median(numeric_only=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.std(numeric_only=True)  # standard deviation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.quantile(0.33, numeric_only=True)  # quantiles of the destributions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.sum(numeric_only=True) # remove `numeric_only=True` to see how strings are summed ... "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `.describe()` method quickly gives an overview on different statistics:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Combining this with a simple query is already quite powerful to get specific statistics, e.g. on a year:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"Year == 2010\").describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or on a specific region:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"Region == 'Region of the Americas'\").describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tocontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Grouping data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What if we would want statistics for all regions? Should we write a loop?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To perform operations/calculations on a specific *group* in the dataset, we can use the `.groupby()` method.\n",
    "\n",
    "Grouping alone does not do much:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "groups_regions = WHO_data.groupby(\"Region\")\n",
    "groups_regions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, we can now perform statistical analysis with the groups, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "groups_regions.mean(numeric_only=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With groups we can ask more specific questions:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is the variability of male life expectancy by region?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "groups_regions['Male'].std()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What was the mean expenditure in each country in the years 2005 to 2010?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"Year > 2004 and Year < 2011\").groupby(\"Country\")[\"Expenditure\"].mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And so on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tocontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We end this peek into the Pandas library with some examples of the built-in plotting functionalities.\n",
    "\n",
    "Each series and dataframe comes with a `.plot()` method. The columns you want to plot are passed as `x` and `y` keyword arguments."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a timeseries of the expenditure in Germany:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"Country == 'Germany'\").plot(\n",
    "    x=\"Year\", y=\"Expenditure\", title=\"Germany\"\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A scatterplot of expenditure versus life expectancy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"Country == 'Germany'\").plot.scatter(\n",
    "    x=\"Both sexes\", y=\"Expenditure\", title=\"Germany\"\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A scatterplot of life expectancy versus expenditure in Europe in the year 2010:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.query(\"Region == 'European Region' and Year == 2010\").plot.scatter(\n",
    "    x=\"Expenditure\", y=\"Both sexes\", title=\"Europe, 2010\"\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A bar chart of the mean expenditure by country:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.groupby(\"Region\").mean(numeric_only=True)[\"Expenditure\"].plot.bar();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And, as a last example, histograms of life expectancies by region:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data.hist(\n",
    "    \"Both sexes\",\n",
    "    by=\"Region\",\n",
    "    alpha=0.3,\n",
    "    legend=True,\n",
    "    figsize=(8, 6),\n",
    "    density=True\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tocontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&rarr; next notebook: holoviews_basic.ipynb](holoviews_basic.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "# Solutions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# life expectancy for both sexes at least 80 years\n",
    "WHO_data[WHO_data[\"Both sexes\"] >= 80]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# United States and year at least 2011\n",
    "WHO_data[\n",
    "    (WHO_data[\"Country\"] == \"United States of America\") & (WHO_data[\"Year\"] > 2010)\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# expenditure at least 500 US$ in the \"African Region\" and the \"Eastern Mediterranean Region\"\n",
    "WHO_data.query(\"Expenditure >= 500 and (Region == 'African Region' or Region == 'Eastern Mediterranean Region')\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# alternative with method chaining\n",
    "WHO_data.query(\"Expenditure >= 500\").query(\"Region == 'African Region' or Region == 'Eastern Mediterranean Region'\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# alternative with `in` keyword\n",
    "WHO_data.query(\"Expenditure >= 500\").query(\"Region in ['African Region', 'Eastern Mediterranean Region']\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  },
  "toc-showtags": false
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
